package com.example.bookservice.service.impl;

import com.example.bookservice.entity.Category;
import com.example.bookservice.handler.ResponseHandler;
import com.example.bookservice.repo.CategoryRepo;
import com.example.bookservice.service.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Mock
    CategoryRepo categoryRepo;
    @Mock
    BookService bookService;

    @Test
    void getAllCategories_Success() {
        List<Category> categoryList = createCategoryList();
        when(categoryRepo.findAll()).thenReturn(categoryList);
        assertEquals(categoryService.getAllCategories(),categoryList);
    }

    @Test
    void getAllCategories_Fail(){
        when(categoryRepo.findAll()).thenThrow(new IllegalArgumentException());
        assertEquals(categoryService.getAllCategories().size(),0);


    }

    private List<Category> createCategoryList() {
        List<Category> categoryList= new ArrayList<>();
        categoryList.add(createCategoryObj());
        return categoryList;
    }

    private Category createCategoryObj() {
        return Category.builder()
                .categoryId(0)
                .categoryName("HORROR")
                .build();
    }

    @Test
    void createCategory_Success() {
        when(categoryRepo.save(createCategoryObj())).thenReturn(createCategoryObj());
        assertEquals(categoryService.createCategory("HORROR"),"Created");
    }

    @Test
    void createCategory_Fail() {
        when(categoryRepo.save(createCategoryObj())).thenThrow(new IllegalArgumentException());
        assertNotEquals(categoryService.createCategory("HORROR"),"Created");
    }

    @Test
    void deleteCategory() {
        assertEquals(categoryService.deleteCategory(1),"Category Not Found");
    }

    @Test
    void deleteCategory_Success(){
        Category category = new Category(1, "categoryName");
        when(categoryRepo.findById(1)).thenReturn(Optional.of(category));
        when(bookService.deleteBookByCategoryId(category)).thenAnswer(invocationOnMock ->
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.OK.value(), "Book Deleted Successfully"), HttpStatus.OK));

        assertEquals(categoryService.deleteCategory(1),"Category Deleted");
    }

    @Test
    void deleteCategory_Error(){
        Category category = new Category(1, "categoryName");
        when(categoryRepo.findById(1)).thenReturn(Optional.of(category));
        when(bookService.deleteBookByCategoryId(category)).thenAnswer(invocationOnMock ->
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR));

        assertEquals(categoryService.deleteCategory(1),HttpStatus.INTERNAL_SERVER_ERROR.toString());
    }

    @Test
    void updateCategory() {
        when(categoryRepo.findById(1)).thenReturn(Optional.of(createCategoryObj()));
        assertEquals(categoryService.updateCategory(1,"COMEDY"),"Updated");
    }

    @Test
    void updateCategory_notFound() {
        assertEquals(categoryService.updateCategory(1,"COMEDY"),"Category Not Found");
    }

    @Test
    void updateCategory_exception() {
        when(categoryRepo.findById(1)).thenThrow(new IllegalArgumentException());
        assertEquals(categoryService.updateCategory(1,"COMEDY"),"java.lang.IllegalArgumentException");
    }
}