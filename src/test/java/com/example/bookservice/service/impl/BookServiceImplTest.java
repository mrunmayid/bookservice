package com.example.bookservice.service.impl;

import com.example.bookservice.commons.BookReqDto;
import com.example.bookservice.commons.BookUpdateDto;
import com.example.bookservice.entity.Book;
import com.example.bookservice.entity.Category;
import com.example.bookservice.repo.BookRepo;
import com.example.bookservice.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {

    @InjectMocks
    BookServiceImpl bookService;

    @Mock
    BookRepo bookRepo;

    @Mock
    CategoryService categoryService;

    @Test
    void deleteBookByCategoryId(){
        assertEquals(bookService.deleteBookByCategoryId(new Category(1,"categoryName")).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void getBookByName_success() {
        when(bookRepo.findAllByBookName("bookName")).thenReturn(createListOfBook());
        assertEquals(HttpStatus.FOUND,bookService.getBookByName("bookName").getStatusCode());
    }

    @Test
    void getBookByName_NotFound() {
        when(bookRepo.findAllByBookName("bookName")).thenReturn(new ArrayList<>());
        assertEquals(HttpStatus.NOT_FOUND,bookService.getBookByName("bookName").getStatusCode());
    }

    @Test
    void getBookByName_Exception() {
        when(bookRepo.findAllByBookName("bookName")).thenThrow(RuntimeException.class);;
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.getBookByName("bookName").getStatusCode());
    }

    @Test
    void getBookByCategory_success() {

        when(bookRepo.findAllByCategoryName(1)).thenReturn(createListOfBook());
        when(categoryService.findByName("categoryName")).thenReturn(new Category(1,"categoryName"));
        assertEquals(HttpStatus.FOUND,bookService.getBookByCategory("categoryName").getStatusCode());
    }

    @Test
    void getBookByCategory_NotFound() {
        when(categoryService.findByName("categoryName")).thenReturn(null);
        assertEquals(HttpStatus.NOT_FOUND,bookService.getBookByCategory("categoryName").getStatusCode());
    }

    @Test
    void getBookByCategory_Exception() {
        when(categoryService.findByName("categoryName")).thenThrow(RuntimeException.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.getBookByCategory("categoryName").getStatusCode());
    }
    @Test
    void getBookByAuthor_Success() {
        when(bookRepo.findAllByAuthor("author")).thenReturn(createListOfBook());
        assertEquals(HttpStatus.FOUND,bookService.getBookByAuthor("author").getStatusCode());
    }

    @Test
    void getBookByAuthor_NotFound() {
        when(bookRepo.findAllByAuthor("author")).thenReturn(new ArrayList<>());
        assertEquals(HttpStatus.NOT_FOUND,bookService.getBookByAuthor("author").getStatusCode());
    }

    @Test
    void getBookByAuthor_Exception() {
        when(bookRepo.findAllByAuthor("author")).thenThrow(RuntimeException.class);;
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.getBookByAuthor("author").getStatusCode());
    }


    private List<Book> createListOfBook() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(createBook());
        return bookList;
    }

    private Book createBook() {
        return Book.builder()
                .bookId(0)
                .bookName("bookName")
                .description("description")
                .categoryId(new Category(1,"categoryName"))
                .bookISBN("ISBN-2010000001")
                .author("author")
                .quantity(1)
                .price(1230L)
                .soldCopies(10)
                .bookImage(null)
                .build();
    }

    @Test
    void saveBook_created() {
        BookReqDto bookReqDto = createBookReqDto();
        Book book = createBook();
        when(categoryService.findByName("categoryName".toUpperCase())).thenReturn(new Category(1,"categoryName"));
        when(bookRepo.save(book)).thenReturn(book);
        assertEquals(HttpStatus.CREATED,bookService.saveBook(bookReqDto,"categoryName").getStatusCode());
    }

    @Test
    void saveBook_NullCategory() {
        BookReqDto bookReqDto = createBookReqDto();
        Book book = createBook();
        when(categoryService.findByName("categoryName".toUpperCase())).thenReturn(null);
        assertEquals(HttpStatus.NOT_FOUND,bookService.saveBook(bookReqDto,"categoryName").getStatusCode());
    }

    @Test
    void saveBook_Category_Exception() {
        BookReqDto bookReqDto = createBookReqDto();
        Book book = createBook();
        when(categoryService.findByName("categoryName".toUpperCase())).thenReturn(null);
        assertEquals(HttpStatus.NOT_FOUND,bookService.saveBook(bookReqDto,"categoryName").getStatusCode());
    }
    @Test
    void saveBook_Exception() {
        when(categoryService.findByName("categoryName".toUpperCase())).thenThrow(new RuntimeException());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.saveBook(new BookReqDto(),"categoryName").getStatusCode());
    }

    private BookReqDto createBookReqDto() {
        return BookReqDto.builder()
                .bookName("bookName")
                .author("author")
                .categoryName("categoryName")
                .bookISBN("ISBN-2010000001")
                .description("description")
                .price(1230L)
                .soldCopies(10)
                .bookImage(null)
                .quantity(1)
                .build();
    }

    @Test
    void deleteBook_success(){
        when(bookRepo.findById(1)).thenReturn(Optional.ofNullable(createBook()));
        assertEquals(HttpStatus.OK,bookService.deleteBook(1).getStatusCode());
    }
    @Test
    void deleteBook_NoBookFound(){
        when(bookRepo.findById(1)).thenReturn(Optional.empty());
        assertEquals(HttpStatus.NOT_FOUND,bookService.deleteBook(1).getStatusCode());
    }


    @Test
    void deleteBook_Exception(){
        when(bookRepo.findById(1)).thenReturn(null);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.deleteBook(1).getStatusCode());
    }

    @Test
    void updateBook(){
        when(bookRepo.findById(1)).thenReturn(Optional.ofNullable(createBook()));
        assertEquals(HttpStatus.OK,bookService.updateBook(1,new BookUpdateDto()).getStatusCode());
    }

    @Test
    void updateBook_NoFound(){
        when(bookRepo.findById(1)).thenReturn(Optional.empty());
        assertEquals(HttpStatus.NOT_FOUND,bookService.updateBook(1,new BookUpdateDto()).getStatusCode());
    }

    @Test
    void updateBook_Exception(){
        when(bookRepo.findById(1)).thenReturn(null);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,bookService.updateBook(1,new BookUpdateDto()).getStatusCode());
    }

}