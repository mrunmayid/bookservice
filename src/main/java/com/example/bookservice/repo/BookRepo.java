package com.example.bookservice.repo;

import com.example.bookservice.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepo extends JpaRepository<Book,Integer> {
    @Modifying
    @Query(value = "DELETE FROM Books b WHERE b.category_id = :category",
            nativeQuery = true)
    void deleteBookByCategory(int category);

    @Query(
            value = "SELECT * FROM books b WHERE b.book_name = :bookName",
            nativeQuery = true)
    List<Book> findAllByBookName(String bookName);
    @Query(
            value = "SELECT * FROM books b WHERE b.category_id = :category",
            nativeQuery = true)
    List<Book> findAllByCategoryName(int category);

    @Query("SELECT b FROM Book b WHERE b.author = :bookAuthorName")
    List<Book> findAllByAuthor(String bookAuthorName);

}
