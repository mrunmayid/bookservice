package com.example.bookservice.commons;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookReqDto {
    @NotBlank(message = "Invalid bookName: Empty bookName")
    @NotNull(message = "Invalid bookName: Blank bookName")
    private String bookName;

    @NotBlank(message = "Invalid categoryName: Empty categoryName")
    @NotNull(message = "Invalid categoryName: Blank categoryName")
    private String categoryName;

    private String description;

    @NotBlank(message = "Invalid bookISBN: Empty bookISBN")
    @NotNull(message = "Invalid bookISBN: Blank bookISBN")
    private String bookISBN;

    @NotBlank(message = "Invalid author: Empty author")
    @NotNull(message = "Invalid author: Blank author")
    private String author;

    @Min(1)
    private Long price;

    @Min(1)
    private int quantity;

    private String bookImage;

    @Min(0)
    private int soldCopies;
}
