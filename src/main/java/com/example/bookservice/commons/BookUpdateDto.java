package com.example.bookservice.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookUpdateDto {
    private String bookName;
    private String description;
    private String bookISBN;
    private String author;
    private Long price;
    private int quantity;
    private String bookImage;
    private int soldCopies;

}
