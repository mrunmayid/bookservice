package com.example.bookservice.commons;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookRespDto {
    private int bookId;
    private String bookName;
    private String categoryName;
    private String description;
    private String author;
    private Long price;
    private int quantity;
    private String bookImage;
    private int soldCopies;
}
