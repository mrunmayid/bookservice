package com.example.bookservice.service.impl;

import com.example.bookservice.commons.BookReqDto;
import com.example.bookservice.commons.BookRespDto;
import com.example.bookservice.commons.BookUpdateDto;
import com.example.bookservice.entity.Book;
import com.example.bookservice.entity.Category;
import com.example.bookservice.handler.ResponseHandler;
import com.example.bookservice.mapper.MapObject;
import com.example.bookservice.repo.BookRepo;
import com.example.bookservice.service.BookService;
import com.example.bookservice.service.CategoryService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.reactive.TransactionContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepo bookRepo;

    @Autowired
    CategoryService categoryService;

    Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);
    @Modifying
    @Override
    public ResponseEntity<?> deleteBookByCategoryId(Category category) {
        try {

            bookRepo.deleteBookByCategory(category.getCategoryId());
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.OK.value(), "Book Deleted Successfully"), HttpStatus.OK);
        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> getBookByName(String bookName) {
        try{
            List<Book> books = bookRepo.findAllByBookName(bookName);
            return responseList(books);
        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> getBookByCategory(String categoryName) {
        try{
            Category category = categoryService.findByName(categoryName);

            if (category!=null){
                List<Book> books = bookRepo.findAllByCategoryName(category.getCategoryId());
                return responseList(books);
            }
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(),"Category Not Found"),HttpStatus.NOT_FOUND);
        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
        }    }

    @Override
    public ResponseEntity<?> getBookByAuthor(String authorName) {
        try{
            List<Book> books =  bookRepo.findAllByAuthor(authorName);
            return responseList(books);
        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> saveBook(BookReqDto bookReq, String categoryName) {
        try{
            Category category = categoryService.findByName(categoryName.toUpperCase());
            if(category==null){
                return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(), "No Category Available"), HttpStatus.NOT_FOUND);
            }
            logger.info("Category Present "+category.getCategoryName());
            Book saveBook = MapObject.mapBookReqDtoToBook(bookReq,category);
            Book book = bookRepo.save(saveBook);
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.CREATED.value(),"Book id "+book.getBookId()),HttpStatus.CREATED);
        }catch (Exception e){
            e.fillInStackTrace();
            logger.info(e.toString());
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> deleteBook(int bookId) {
        logger.info("Book Id"+bookId);
        try {
            Optional<Book> book = bookRepo.findById(bookId);
            if (book.isEmpty()){
                return new ResponseEntity<>
                        (new ResponseHandler<>(HttpStatus.NOT_FOUND.value(), "Book Not Found"),HttpStatus.NOT_FOUND);
            }
            bookRepo.deleteById(bookId);
            return new ResponseEntity<>
                    (new ResponseHandler<>(HttpStatus.OK.value(), "Book Deleted Successfully"),HttpStatus.OK);

        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @Override
    public ResponseEntity<?> updateBook(int bookId, BookUpdateDto dto) {
        try {
            Optional<Book> book = bookRepo.findById(bookId);
            if (book.isEmpty()){
                return new ResponseEntity<>
                        (new ResponseHandler<>(HttpStatus.NOT_FOUND.value(), "Book Not Found"),HttpStatus.NOT_FOUND);
            }
            int counter = MapObject.BookUpdateDtoToBook(dto,book.get());
            bookRepo.save(book.get());
            return new ResponseEntity<>
                    (new ResponseHandler<>(HttpStatus.OK.value(), "Book Updated with "+counter+" Fields"),HttpStatus.OK);
        }catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity<?> responseList(List<Book> books) {
        if(!books.isEmpty()) {
            List<BookRespDto> respDtos = new ArrayList<>();
            for (Book book:books){
                if(book.getQuantity()>0){
                    respDtos.add(MapObject.mapBookToBookRespDto(book));
                }
            }
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.FOUND.value(),respDtos),HttpStatus.FOUND);
        }
        return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(),"No Book Found"),HttpStatus.NOT_FOUND);
    }
}
