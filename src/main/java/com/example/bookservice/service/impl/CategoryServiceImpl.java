package com.example.bookservice.service.impl;

import com.example.bookservice.entity.Category;
import com.example.bookservice.repo.CategoryRepo;
import com.example.bookservice.service.BookService;
import com.example.bookservice.service.CategoryService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepo categoryRepo;

    @Autowired
    BookService bookService;

    @Override
    public List<Category> getAllCategories() {
        List<Category> categoryList= new ArrayList<>();
        try {
            categoryList = categoryRepo.findAll();
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return categoryList;
    }

    @Override
    public String createCategory(String categoryName) {
        Object obj = addCategory(categoryName);
        if (obj instanceof Category){
            return "Created";
        }
        return obj.toString();
    }

    @Override
    @Transactional
    public String deleteCategory(int categoryId) {
        try {
            Optional<Category> category = categoryRepo.findById(categoryId);
            if (!category.isPresent()){
                return "Category Not Found";
            }
            ResponseEntity <?> response = bookService.deleteBookByCategoryId(category.get());
            if(response.getStatusCode()== HttpStatus.INTERNAL_SERVER_ERROR){
                return response.getStatusCode().toString();
            }
            categoryRepo.deleteById(categoryId);
            return "Category Deleted";
        }catch (Exception e){
            e.fillInStackTrace();
            return e.toString();
        }
    }

    @Override
    @Transactional
    public String updateCategory(int categoryId, String newCategoryName) {
        try {
            Optional<Category> category = categoryRepo.findById(categoryId);
            if (category.isPresent()){
                if (newCategoryName.equals(category.get().getCategoryName())){
                    return "Already Updated";
                }
                category.get().setCategoryName(newCategoryName);
                categoryRepo.save(category.get());
                return "Updated";
            }
            else {
                return "Category Not Found";
            }
        }catch (Exception e){
            e.fillInStackTrace();
            return e.toString();
        }
    }

    @Transactional
    @Override
    public Object addCategory(String name) {
        Category category = categoryRepo.findByCategoryName(name);
        if(category!=null){
            return "Category Present";
        }
        category = new Category();
        category.setCategoryName(name.toUpperCase());
        try{
            return categoryRepo.save(category);

        }catch(Exception e){
            e.fillInStackTrace();
            return e.toString();
        }
    }

    @Override
    public Category findByName(String categoryName) {
        return categoryRepo.findByCategoryName(categoryName);
    }


}
