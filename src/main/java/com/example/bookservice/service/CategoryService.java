package com.example.bookservice.service;

import com.example.bookservice.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();

    String createCategory(String categoryName);

    String deleteCategory(int categoryId);

    String updateCategory(int categoryId, String newCategoryName);

    Object addCategory(String name);

    Category findByName(String categoryName);

}
