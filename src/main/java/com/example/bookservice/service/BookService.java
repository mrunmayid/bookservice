package com.example.bookservice.service;

import com.example.bookservice.commons.BookReqDto;
import com.example.bookservice.commons.BookUpdateDto;
import com.example.bookservice.entity.Category;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface BookService {
    public ResponseEntity<?> deleteBookByCategoryId(Category category);

    public ResponseEntity<?> getBookByName(String bookName);

    public ResponseEntity<?> getBookByCategory(String categoryName);

    public ResponseEntity<?> getBookByAuthor(String authorName);

    ResponseEntity<?> saveBook(BookReqDto book, String category);

    ResponseEntity<?> deleteBook(int bookId);

    ResponseEntity<?> updateBook(int bookId, BookUpdateDto dto);
}
