package com.example.bookservice.controller;

import com.example.bookservice.commons.BookReqDto;
import com.example.bookservice.commons.BookUpdateDto;
import com.example.bookservice.handler.ResponseHandler;
import com.example.bookservice.service.BookService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping("/getbook")
    public ResponseEntity<?> getBookDetails(@RequestHeader(required = false,defaultValue = "bookName") String bookName,
                                            @RequestHeader(required = false,defaultValue = "categoryName") String categoryName,
                                            @RequestHeader(required = false,defaultValue = "authorName") String authorName){
        ResponseEntity<?> response;
        if (!bookName.equals("bookName")){
            response = bookService.getBookByName(bookName);
        } else if (!categoryName.equals("categoryName")) {
            response = bookService.getBookByCategory(categoryName);
        } else if (!authorName.equals("authorName")) {
            response = bookService.getBookByAuthor(authorName);
        }else {
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.BAD_REQUEST.value(),"Empty Fields"),HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @PostMapping("/savebook")
    public ResponseEntity<?> saveBookDetails(@Valid @RequestBody(required = true ) BookReqDto book, BindingResult bindingResult){
        return !bindingResult.hasErrors()?
                bookService.saveBook(book,book.getCategoryName()) :
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.BAD_REQUEST.value(),"Missing Field"),HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/deletebook")
    public ResponseEntity<?> deleteBook(@RequestHeader int bookId){
        return bookService.deleteBook(bookId);
    }

    @PutMapping("/updatebook")
    public ResponseEntity<?> updateBook(@RequestHeader int bookId, @RequestBody BookUpdateDto dto){
        return bookService.updateBook(bookId,dto);
    }
}
