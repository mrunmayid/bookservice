package com.example.bookservice.controller;

import com.example.bookservice.entity.Category;
import com.example.bookservice.handler.ResponseHandler;
import com.example.bookservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/getall")
    public ResponseEntity<?> getCategory(){
        List<Category> list = categoryService.getAllCategories();
        if(!list.isEmpty()){
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.FOUND.value(),list),HttpStatus.FOUND);
        }
        return  new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(),"No Category Available"),HttpStatus.NOT_FOUND);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createCategory(@RequestHeader String categoryName){
        if(categoryName.isEmpty() || categoryName.equals("null")){
            return new ResponseEntity<>(new ResponseHandler<>(HttpStatus.BAD_REQUEST.value(),"Missing Fields"),HttpStatus.BAD_REQUEST);
        }
        String result = categoryService.createCategory(categoryName);
        return result.equals("Created")?
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.CREATED.value(),"New Category Added"),HttpStatus.CREATED):
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),result),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteCategory(@RequestHeader int categoryId){
        String result = categoryService.deleteCategory(categoryId);
        return result.equals("Category Deleted")?
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.OK.value(),"Category Deleted"),HttpStatus.OK):
                result.equals("Category Not Found")?
                        new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(),result),HttpStatus.NOT_FOUND):
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),result),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateCategory(@RequestHeader int categoryId,@RequestHeader String newCategoryName){
        String result = categoryService.updateCategory(categoryId,newCategoryName);
        return result.equals("Updated")?
                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.OK.value(),"Category Updated"),HttpStatus.OK):
                result.equals("Category Not Found")?
                        new ResponseEntity<>(new ResponseHandler<>(HttpStatus.NOT_FOUND.value(),result),HttpStatus.NOT_FOUND):
                        result.equals("Already Updated")?
                        new ResponseEntity<>(new ResponseHandler<>(HttpStatus.BAD_REQUEST.value(),result),HttpStatus.BAD_REQUEST):
                                new ResponseEntity<>(new ResponseHandler<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),result),HttpStatus.INTERNAL_SERVER_ERROR);


    }

}
