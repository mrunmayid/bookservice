package com.example.bookservice.mapper;

import com.example.bookservice.commons.BookReqDto;
import com.example.bookservice.commons.BookRespDto;
import com.example.bookservice.commons.BookUpdateDto;
import com.example.bookservice.entity.Book;
import com.example.bookservice.entity.Category;

public class MapObject {
    public static BookRespDto mapBookToBookRespDto(Book book){
        return BookRespDto.builder()
                .bookId(book.getBookId())
                .bookName(book.getBookName())
                .author(book.getAuthor())
                .categoryName(book.getCategoryId().getCategoryName())
                .description(book.getDescription())
                .author(book.getAuthor())
                .price(book.getPrice())
                .quantity(book.getQuantity())
                .soldCopies(book.getSoldCopies())
                .build();
    }

    public static Book mapBookReqDtoToBook(BookReqDto book, Category category){
        return Book.builder()
                .author(book.getAuthor())
                .bookName(book.getBookName())
                .categoryId(category)
                .description(book.getDescription())
                .price(book.getPrice())
                .quantity(book.getQuantity())
                .soldCopies(book.getSoldCopies())
                .bookISBN(book.getBookISBN())
                .build();
    }

    public static int BookUpdateDtoToBook(BookUpdateDto dto, Book book) {
        int counter = 0;
        if (dto!=null){

            if(dto.getBookName()!=null) {
                counter++;
                book.setBookName(dto.getBookName());
            }

            if(dto.getAuthor()!=null) {
                book.setAuthor(dto.getAuthor());
                counter++;
            }

            if(dto.getDescription()!=null) {
                book.setDescription(dto.getDescription());
                counter++;
            }

            if(dto.getPrice()!=null){
                counter++;
                book.setPrice(dto.getPrice());
            }

            if(dto.getQuantity()>0){
                counter++;
                book.setQuantity(dto.getQuantity());
            }

            if(dto.getSoldCopies()>0) {
                counter++;
                book.setSoldCopies(dto.getSoldCopies());
            }

            if(dto.getBookISBN()!=null) {
                counter++;
                book.setBookISBN(dto.getBookISBN());
            }
        }
        return counter;
    }
}
