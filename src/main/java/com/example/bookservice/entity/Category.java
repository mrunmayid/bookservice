package com.example.bookservice.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "category_details")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    @Id
    int categoryId;

    @Column(unique = true)
    String categoryName;
}

