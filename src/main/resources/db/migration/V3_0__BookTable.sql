CREATE TABLE books (
book_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
author VARCHAR(255),
bookisbn VARCHAR(255) NOT NULL UNIQUE,
book_image VARCHAR(255),
book_name VARCHAR(255) NOT NULL,
description VARCHAR(255),
price BIGINT NOT NULL,
quantity INT NOT NULL,
sold_copies INT NOT NULL,
category_id INT,
FOREIGN KEY (category_id) REFERENCES category_details(category_id)
);

INSERT INTO books
(author,bookisbn,book_name,description,price,quantity,sold_copies,category_id)
VALUES ('XYZ', '1234', 'bookName', 'description', '1200', '1', '23', '1');
