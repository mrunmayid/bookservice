CREATE TABLE category_details(
category_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
category_name VARCHAR(255) UNIQUE);

INSERT INTO category_details (category_name) VALUES ('COMEDY');